# README #

### What is this repository for? ###
This repo contains Machine learning on Vision using opencv library


### In this notebook we are performing clusting of image using kmeans

#### Steps
* Reading images and converting them to BGR to RGB
* Fetching red green blue color from each individual pixel
* Training our model using kmeans
* Storing the error and cluster size in a data frame
* Plotting the error and interia of our Kmeans model
* Finding the best K from the graph
* using the best K to fit our data and perform clustring on our image
* Reducing the less dominant colors from the image 
* Displaying the output image


