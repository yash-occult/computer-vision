# README #

### What is this repository for? ###
This repo contains basic image processing using opencv and pil library


## Filters 

#### We use two types of filters in image processing ###
* Low Pass Filter :- Used for removing noise or blurring image.
* High Pass Filter :- Used for adding sharpness to image or finding corners in an image.

## Linear Filter

#### Linear Filter :- It is a method where-in the outut value of pixel depends on the linear combination of the neighbouring input pixels. With a linear filter we can perform the following methods on an image:-

* Blur
* Sharpen
* Edge
* Detect
* Contour


## Blur
#### There exists four basic types of blurring in image processing. They are the following :-

* Averaging : As the name suggest we pass a filter of let say 5x5, It takes the average of all the pixel that lie under our 5x5 kernel and then it replaces the center element.

* Gaussian Blurring : Gaussian blur is another intresting blurring technique. It can used to blur the image and remove noise from the image. In gaussian blur, Those pixel that are in the center of image are given more weightage than the one's far away from the center. It is important to note that gaussian blur takes a positive and odd number as its kernel.

* Median Blurring : In meidan blurring, Median is calculated for all the pixel that fall under the kernel and the centeral element is replaced with the median value. It can be used for blurring or removing noise from the image.

* Bilateral Blurring : Bilateral filter removes noise and also preserves the sharpness of edges. In gaussian blur it doesnt cares about the intensity of neighbouring pixels. The Gaussian function of space makes sure that only nearby pixels are considered for blurring, while the Gaussian function of intensity difference makes sure that only those pixels with similar intensities to the central pixel are considered for blurring

## Sharp
* For sharpning of image we will create a custom filter and apply it on image.

## Edge detection
#### Edge detection can we done using canny detection and custom kernels.

* Custom kernel : kernel used here is [[0,-1,0], [-1,4,-1], [0,-1,0]] and then passing it in filter2d to detect edges.

* Canny edge detection : canny edge detection work with 4 steps, first the image is made smooth and removed noise using gaussian filter. After that it identify the areas in the image with the strongest intensity gradients. Apply non-maximum suppression to thin out the edges. Thresholding with Hysteresis

## Contour detection
* Performed contour detection using opencv built-in functions.
* Created a binary inverted color image using the original image.
* Using findContours and CHAIN_APPROX_SIMPLE we creatde contour of our image.
* Using drawContours we add the countour to our image


## Morphology
* Performed Morphology using opencv built-in functions.
* Created a mask using the binary inversion of the input image.
* Created a kernel of 5x5.
* Used 6 types of morpholoy techniques.
* Dilation, Erosion, Opening, Closing, Gradient, Tophat

## Blending image
* Performed Blending using using PIL library.
* Created template image and rezied it with the original image to match pixels.
* Created a mask with the size of main image
* Created a circular mask and performed composition on main image, templated image and mask

## Templating
* Performed templating using using opencv library.
* Read a main image and a template image.
* Converted both the images to gray scale and resized them for faster processing.
* using .matchTemplate we pass in the pixel that are needed to match the main image


