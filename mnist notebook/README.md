# README #

### What is this repository for? ###
This repo contains training mnist dataset with simple neural network and with convolutional neural network


### Training with neural network ###

##### Steps

* Load the mnist dataset using keras.
* Flattering input image from (60000, 28, 28) to (60000, 784).
* Converting the data to float32 type as numpy creates float64, Whereas float32 is accepted by keras model's layers.
* Normalizing the input data 
* Converting the output lables to one hot encoding
* Creating Sequantial model having actiavtion as relu, input_dimension = 784 and number of neurons = 784
* At output layer defining 10 output classes and activation as softmax
* Plotting train vs test accuracy 
* Plotting train vs test loss
* Saved the model

### Simple neural network model  performance ###

##### Model with epoch of 10, SGD as optimizer

* Time taken by this model : 34.93 seconds
* loss: 0.1502
* accuracy: 0.9582 
* val_loss: 0.1487 
* val_accuracy: 0.9563

![Accuracy of model](images/nn1_acc.png)
![Loss of model](images/nn1_loss.png)


##### Model with epoch of 10, Adam as optimizer and batch_size of 200

* Time taken by this model : 14.12 seconds
* loss: 0.0085
* accuracy: 0.9985 
* val_loss: 0.0611 
* val_accuracy: 0.9815

![Accuracy of model](images/nn2_acc.png)
![Loss of model](images/nn2_loss.png)

### Convolutional neural network ###

##### Model with epoch of 15, Adam as optimizer

* Two layers of Convolution layer with size (3,3) and activation layer as relu
* Two layers of MaxPool layer with size (2,2)
* Flattering layer
* Dropout of 0.5 
* Output layer with a activation as softmax 

![Model summary](images/cnn_model_summary.png)

### Convolutional neural network  model performace ###

* Time taken by this model : 231.19 seconds
* loss: 0.0359 
* accuracy: 0.9885 
* val_loss: 0.0318 
* val_accuracy: 0.9910

![Accuracy of model](images/cnn_acc.png)
![Loss of model](images/cnn_loss.png)