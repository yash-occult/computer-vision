# README #

### What is this repository for? ###
This repo contains feature extraction using opencv library


### In this notebook we used two algorithms using opencv SIFT( Scale Invariant Feature Transform ) and ORB( Orinted Fast and Rotated brief )

#### SIFT Algorithm 
* SIFT algorithm took a total time of 4.27 seconds to detect and match features.
* SIFT algorithm found a total 3710 matches in features.

#### ORB Algorithm 
* ORB algorithm took a total time of 0.319 seconds to detect and match features.
* ORB algorithm found a total 500 matches in features.

#### Observation 
<p>Time Comparison</p>
* SIFT took much time compared to ORB.

<p>Matches Comparison</p>
* SIFT found more number of matches as compared to ORB. But SIFT includes false detection as well.
* ORB performs better in terms of true detection of features.
* SIFT detects more number of features which can be adjusted using threshold.



